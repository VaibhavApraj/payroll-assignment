import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {path:'auth',loadChildren:()=>import('../app/views/pages/auth/auth.module').then(m=>m.AuthModule)},
  {path:'',loadChildren:()=>import('../app/views/pages/main/main.module').then(m=>m.MainModule)},
  {path:'**',redirectTo:'page-not-found',pathMatch:'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { 

}
