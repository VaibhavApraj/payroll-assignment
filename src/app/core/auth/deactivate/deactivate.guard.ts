import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanDeactivate, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { AddTaskComponent } from 'src/app/views/pages/main/add-task/add-task.component';

@Injectable({
  providedIn: 'root'
})
export class DeactivateGuard implements CanDeactivate <AddTaskComponent>{
  canDeactivate(component: AddTaskComponent, currentRoute: ActivatedRouteSnapshot, currentState: RouterStateSnapshot, nextState?: RouterStateSnapshot | undefined): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
    //return window.confirm('Do you want to leave this page?');
    return component.canExit();
  }
  
}
