import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TaskService {

  constructor(private http:HttpClient) { }

  findMyTask(from: number, to: number, title: string, userId: any, isArchive: boolean, userIds: string, priority: string, StatusIds: string, FromDate: string, ToDate: string, SortColumn: string, SortOrder: string): Observable<any> {
    const params = {
      From: from,
      To: to,
      Title: title,
      UserId: '',
      IsArchive: isArchive,
      UserIds: [],
      Priority: priority,
      TaskStatus: StatusIds,
      FromDueDate: FromDate,
      ToDueDate: ToDate,
      SortByDueDate: '',
    }
    return this.http.post('api/Task/UserTasksAssignedByMe', params)
      .pipe(
        map(res => res)
      );
  }
  getCustomerList(params: { From: number; To: number; Text: string; }): Observable<any> {
    return this.http.post('api/CRM/Leads', params)
      .pipe(map(
        res => res
      ))
  }

  getCompanyMembers(from: number, to: number, text: string): Observable<any> {
    return this.http.get('api/CompanyMembers?from=' + from + '&text=' + text + '&to=' + to)
      .pipe(
        map(res => res)
      );
  }

  addTask(taskDetails: any): Observable<any> {
    return this.http.post('api/Task/AssignTask', taskDetails)
      .pipe(
        map(res => res)
      );
  }
  deleteTask(taskId: number): Observable<any> {
    return this.http.get('api/Task/DeleteTask?taskId=' + taskId)
      .pipe(
        map(res => res)
      );
  }
}
